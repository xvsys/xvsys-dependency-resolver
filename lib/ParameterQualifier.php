<?php

/*
 * The MIT License
 *
 * Copyright 2015 Alexander Schlegel.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace xvsys\dependency;

/**
 * Description of ParameterQualifier
 * 
 * @Annotation
 * @Target({"METHOD"})
 * 
 * @author Alexander Schlegel
 */
class ParameterQualifier {

    /**
     *
     * @var array 
     */
    private $concreteClasses = array();

    /**
     * Constructor.
     *
     * @param array $data An array of key/value parameters.
     */
    public function __construct(array $data) {
        if (isset($data['value'])) {
            if (!is_array($data['value'])) {
                throw new \InvalidArgumentException('Parameter of annotation class "ParameterQualifier" is not an array');
            }
            $this->concreteClasses = $data['value'];
        }
    }

    /**
     * 
     * @return array
     */
    public function getConcreteClasses() {
        return $this->concreteClasses;
    }

}
