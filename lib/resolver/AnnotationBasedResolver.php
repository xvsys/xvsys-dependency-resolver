<?php

namespace xvsys\dependency\resolver;

use Doctrine\Common\Annotations\Reader;
use Doctrine\Common\Cache\Cache;

/**
 * Description of AnnotationMapping
 *
 * @author Alexander Schlegel
 */
class AnnotationBasedResolver implements DependencyResolverInterface {

    /**
     *
     * @var Reader 
     */
    private $reader;

    /**
     *
     * @var string 
     */
    private $class;

    /**
     *
     * @var Cache 
     */
    private $cache;

    /**
     *
     * @var array 
     */
    private $classInformation;

    public function __construct(Reader $reader, Cache $cache) {
        $this->reader = $reader;
        $this->cache = $cache;
    }

    public function getConstructorInjectionParamters() {
        return $this->classInformation['construct'];
    }

    public function getPropertyInjections() {
        return $this->classInformation['properties'];
    }

    public function setClass($class) {
        $this->class = $class;
        $this->loadClassInformation();
    }

    public function getPostConstructHandler() {
        return $this->classInformation['postConstruct'];
    }

    private function loadClassInformation() {
        $id = 'controller#' . $this->class . '#information';
        if ($this->cache->contains($id)) {
            $this->classInformation = $this->cache->fetch($id);
        }
        $data = array(
            'construct' => array(),
            'postConstruct' => '',
            'properties' => array()
        );
        // constructor injection paramters
        $class = new \ReflectionClass($this->class);
        $method = $class->getConstructor();
        $qualifier = $this->reader->getMethodAnnotation($method, '\\xvsys\\dependency\\ParameterQualifier');
        if ($qualifier instanceof \xvsys\dependency\ParameterQualifier) {
            $data['construct'] = $qualifier->getConcreteClasses();
        }
        // property injections
        $properties = $class->getProperties();
        foreach ($properties as $property) {
            $injection = $this->reader->getPropertyAnnotation($property, '\\xvsys\\dependency\\Inject');
            if ($injection instanceof \xvsys\dependency\Inject) {
                $data['properties'][$property->getName()] = $injection->getClassName();
            }
        }
        // post constructs
        $methods = $class->getMethods();
        foreach ($methods as $method) {
            $pc = $this->reader->getMethodAnnotation($method, '\\xvsys\\dependency\\PostConstruct');
            if ($pc instanceof \xvsys\dependency\PostConstruct) {
                $data['postConstruct'] = $method->getName();
                break;
            }
        }
        $this->cache->save($id, $data);
        $this->classInformation = $data;
    }

}
